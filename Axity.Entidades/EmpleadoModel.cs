﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axity.Entidades
{
   public class EmpleadoModel
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public DateTime fechaNacimiento { get; set; }
    }
}
