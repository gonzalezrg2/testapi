﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axity.Entidades
{
    public class ResponseModel
    {
        public int ErrorId { get; set; }
        public bool Satisfactorio { get; set; }
        public string Mensaje { get; set; }
    }
}
