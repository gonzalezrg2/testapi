﻿using Axity.Bussines.Interfaces;
using Axity.DATA.Dao;
using Axity.Entidades;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Axity.Bussines.Services
{
    public class EmpleadoService : IEmpleadoService
       
    {
        EmpleadoDao _dao;
        public EmpleadoService(IConfiguration config)
        {
            _dao = new EmpleadoDao(config, "Default");
        }

       
        public string DeleteEmpleado(int id)
        {
            string text = "";
            Dictionary<string, dynamic> P = new Dictionary<string, dynamic>();
            P.Add("id", id);
            var result = _dao.DeleteEmpleado(P);

            if (result.Satisfactorio){
                text = "Empleado eliminado satisfactoriamente";
            }
            else{
                text = result.Mensaje;
            }

            return text;
        }

        public List<EmpleadoModel> GetEmpleados()
        {
            Dictionary<string, dynamic> P = new Dictionary<string, dynamic>();
            var result = _dao.GetEmpleados(P);
            return result;
        }

        
    }
}
