﻿using Axity.Entidades;
using System.Collections.Generic;

namespace Axity.Bussines.Interfaces
{
    public interface IEmpleadoService
    {
        List<EmpleadoModel> GetEmpleados();
        string DeleteEmpleado(int id);
    }
}
