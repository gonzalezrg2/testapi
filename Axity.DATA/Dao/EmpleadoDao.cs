﻿using Axity.Entidades;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace Axity.DATA.Dao
{
    public class EmpleadoDao
    {
        internal DBConnection _db;

        public EmpleadoDao(IConfiguration config)
        {
            _db = new DBConnection(config);
        }

        public EmpleadoDao(IConfiguration config, string con)
        {
            _db = new DBConnection(config, con);
        }


        public List<EmpleadoModel> GetEmpleados(Dictionary<string, dynamic> P)
        {
            return (List<EmpleadoModel>)_db.Query<EmpleadoModel>(P, "[dbo].[spempleado_consultar]");
        }

        public ResponseModel DeleteEmpleado(Dictionary<string, dynamic> P)
        {
            return _db.QuerySingle<ResponseModel>(P, "[dbo].[spempleado_eliminar]");
        }
    }
}
